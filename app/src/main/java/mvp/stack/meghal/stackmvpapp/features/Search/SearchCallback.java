package mvp.stack.meghal.stackmvpapp.features.Search;

import java.util.List;

import mvp.stack.meghal.stackmvpapp.features.Search.Data.SearchData;

/**
 * Created by Meghal on 5/18/2016.
 */
public interface SearchCallback {


    void onTaskCompleted(List<SearchData> searchDataList);

    void onTaskFailed();


}
