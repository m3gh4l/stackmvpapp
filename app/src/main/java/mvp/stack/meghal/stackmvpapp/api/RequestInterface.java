package mvp.stack.meghal.stackmvpapp.api;

import mvp.stack.meghal.stackmvpapp.api.Feed;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Meghal on 5/18/2016.
 */
public interface RequestInterface {

    @GET("/2.2/search?page=1&pagesize=4&order=desc&sort=activity&site=stackoverflow&tagged=java")
    Call<Feed> getData(@Query("title") String tags);
}