package mvp.stack.meghal.stackmvpapp.api;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by Meghal on 5/21/2016.
 */
public interface RequestInterfaceUsingRxJava {

    @GET("/2.2/search/advanced?page=1&pagesize=4&order=desc&sort=activity&site=stackoverflow&tagged=android")
    Observable<Feed> getData(@Query("title") String tags);

}
