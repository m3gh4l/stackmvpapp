package mvp.stack.meghal.stackmvpapp.features.ImageDownloader;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;


/**
 * Created by Meghal on 5/19/2016.
 */
public class ImageDownloader {

    private static final String TAG = "ImageDownloader";
    private static final int IO_BUFFER_SIZE = 4 * 1024;

    private Handler handler;

    public ImageDownloader() {
        handler = new Handler();
    }

    public interface ImageCallback {
        void onImageLoaded(Bitmap bitmap);
    }

    public void loadAsyncBitmap(final String url, final ImageCallback callback) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                final Bitmap bitmap = LoadBitmap(url);
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        callback.onImageLoaded(bitmap);
                    }
                });
            }
        }).start();
    }

    public static Bitmap LoadBitmap(String url) {


        Bitmap bitmap = null;
        InputStream inputStream = null;
        BufferedOutputStream outputStream = null;

        try {

            url = url.replace("\"", "");
            Log.i(TAG, "load image: " + url);
            inputStream = new BufferedInputStream(new URL(url).openStream(), IO_BUFFER_SIZE);
            if (inputStream == null) {
                Log.e(TAG, "inputstream is null");
            }

            final ByteArrayOutputStream dataStream = new ByteArrayOutputStream();

            outputStream = new BufferedOutputStream(dataStream, IO_BUFFER_SIZE);


            byte[] buffer = new byte[1024];
            int len = inputStream.read(buffer);
            while (len != -1) {
                outputStream.write(buffer, 0, len);
                len = inputStream.read(buffer);
            }
            outputStream.flush();


            final byte[] data = dataStream.toByteArray();
            BitmapFactory.Options options = new BitmapFactory.Options();
            //options.inSampleSize = 1;

            bitmap = BitmapFactory.decodeByteArray(data, 0, data.length, options);
        } catch (IOException e) {
            e.printStackTrace();
            Log.e(TAG, "Could not load Bitmap from: " + url);
        } finally {
            try {
                if (inputStream != null) {
                    inputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (outputStream != null) {
                    outputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return bitmap;
        }

    }
}
