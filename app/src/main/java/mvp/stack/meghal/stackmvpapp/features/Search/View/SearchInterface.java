package mvp.stack.meghal.stackmvpapp.features.Search.View;

import java.util.List;

import mvp.stack.meghal.stackmvpapp.features.Search.Data.SearchData;

/**
 * Created by Meghal on 5/17/2016.
 */
public interface SearchInterface {

    void ProgressbarVisible(Boolean visible);

    void setSearchDataList(List<SearchData> searchDataList);

    void showError();

}
