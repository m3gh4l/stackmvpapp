package mvp.stack.meghal.stackmvpapp.features.Search.Data;

import com.google.gson.JsonArray;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by Meghal on 5/17/2016.
 */
public class SearchData {

    private String title;
    private String question_id;
    private String answer_count;
    private String body;
    private String excerpt;

    private int score;

    private UserData owner;

    public SearchData(String title, String username, String question_id, int score, String answer_count, String body, String excerpt, UserData owner) {
        this.title = title;
        this.answer_count = answer_count;
        this.question_id = question_id;
        this.score = score;
        this.body = body;
        this.excerpt = excerpt;
        this.owner = owner;

    }

    public String getQuestion_id() {
        return question_id;
    }

    public String getBody() {
        return body;
    }

    public String getExcerpt() {
        return excerpt;
    }

    public String getAnswer_count() {
        return answer_count;
    }


    public String getTitle() {
        return title;
    }

    public int getScore() {
        return score;
    }

    public UserData getOwner() {
        return owner;
    }
}