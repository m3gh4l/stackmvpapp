package mvp.stack.meghal.stackmvpapp.features.Search;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import mvp.stack.meghal.stackmvpapp.R;
import mvp.stack.meghal.stackmvpapp.features.ImageDownloader.GlideImageLoader;
import mvp.stack.meghal.stackmvpapp.features.ImageDownloader.ImageLoader;
import mvp.stack.meghal.stackmvpapp.features.Search.Data.SearchData;

/**
 * Created by Meghal on 5/17/2016.
 */
public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.MyViewHolder> {

    private static final String TAG = "SearchAdapter";
    private List<SearchData> searchDataList;

    private LayoutInflater layoutInflater;
    private ImageLoader imageLoader;

    protected class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView questionTextView;
        private TextView usernameTextView;
        private TextView votesTextView;
        private TextView excerptTextView;
        private TextView bodyTextView;
        private TextView userIdTextView;
        private TextView totalAnswersTextView;
        private TextView repotationTextView;
        private ImageView imageView;

        private MyViewHolder(View itemView) {
            super(itemView);
            questionTextView = (TextView) itemView.findViewById(R.id.questionTextView);
            usernameTextView = (TextView) itemView.findViewById(R.id.userNameTextView);
            votesTextView = (TextView) itemView.findViewById(R.id.votesTextView);
            imageView = (ImageView) itemView.findViewById(R.id.imageView);
            bodyTextView = (TextView) itemView.findViewById(R.id.body);
            excerptTextView = (TextView) itemView.findViewById(R.id.excerpt);
            userIdTextView = (TextView) itemView.findViewById(R.id.userIdTextView);
            totalAnswersTextView = (TextView) itemView.findViewById(R.id.totalTextView);
            repotationTextView = (TextView) itemView.findViewById(R.id.reputationTextView);

        }
    }


    public SearchAdapter(Context mContext) {

        layoutInflater = LayoutInflater.from(mContext);
        imageLoader = new GlideImageLoader(mContext);

    }


    public void setData(List<SearchData> searchDataList) {
        this.searchDataList = searchDataList;
        if (searchDataList == null) {
            Log.e(TAG, "null data set");
        } else {
            Log.i(TAG, "data size: " + searchDataList.size());
        }

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = layoutInflater.inflate(R.layout.custom_query_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        SearchData searchData = searchDataList.get(position);

        holder.questionTextView.setText(searchData.getTitle());
        holder.totalAnswersTextView.setText("Answers : " + searchData.getAnswer_count());
        holder.votesTextView.setText("Votes : " + String.valueOf(searchData.getScore()));
//        holder.bodyTextView.setText("Body : " + searchData.getBody());
//        holder.excerptTextView.setText("Excerpt : " + searchData.getExcerpt());
        holder.usernameTextView.setText("UserName : " + searchData.getOwner().getDisplay_name());
//        holder.userIdTextView.setText(searchData.getOwner().getUser_id());

        holder.repotationTextView.setText("Repo : " + searchData.getOwner().getReputation());
        imageLoader.loadImage(searchData.getOwner().getProfile_image(), holder.imageView);
        /*

        // This method is for custom Image Loader :)
        imageDownloader.loadAsyncBitmap(searchData.getImage(), new ImageDownloader.ImageCallback() {
            @Override
            public void onImageLoaded(Bitmap bitmap) {
                holder.imageView.setImageBitmap(bitmap);





            }
        });
*/

    }


    @Override
    public int getItemCount() {

        if (searchDataList != null) {
            return searchDataList.size();

        } else {

            return 0;
        }

    }
}
