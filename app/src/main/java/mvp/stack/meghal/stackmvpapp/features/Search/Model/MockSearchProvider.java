package mvp.stack.meghal.stackmvpapp.features.Search.Model;

import java.util.ArrayList;
import java.util.List;

import mvp.stack.meghal.stackmvpapp.features.Search.SearchCallback;
import mvp.stack.meghal.stackmvpapp.features.Search.Data.SearchData;

/**
 * Created by Meghal on 5/18/2016.
 */
public class MockSearchProvider {

    private List<SearchData> searchDataList = new ArrayList<>();


    public void getItems(String tag, SearchCallback listener) {

        prepareMockData();
        listener.onTaskCompleted(searchDataList);


    }

    public void prepareMockData() {
        int i;
        for (i = 0; i < 100; i++) {
            //          SearchData data = new SearchData("What is SOF " + i, "Name " + i,i, String.valueOf(i), i, "ImageLink",null,null,null);
            //         searchDataList.add(data);
        }
    }
}
