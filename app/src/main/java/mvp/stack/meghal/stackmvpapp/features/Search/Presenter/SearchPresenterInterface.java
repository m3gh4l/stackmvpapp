package mvp.stack.meghal.stackmvpapp.features.Search.Presenter;

/**
 * Created by Meghal on 5/17/2016.
 */
public interface SearchPresenterInterface {

    void getResults(String query);

}
