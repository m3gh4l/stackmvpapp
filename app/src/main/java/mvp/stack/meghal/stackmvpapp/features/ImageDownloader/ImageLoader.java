package mvp.stack.meghal.stackmvpapp.features.ImageDownloader;

import android.content.Context;
import android.widget.ImageView;

/**
 * Created by Meghal on 5/19/2016.
 */
public interface ImageLoader {

    void loadImage(String url, ImageView imageView);
}
