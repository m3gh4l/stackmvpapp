package mvp.stack.meghal.stackmvpapp.features.Search.View;


import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

import mvp.stack.meghal.stackmvpapp.features.Search.Model.RetrofitSearchProvider;
import mvp.stack.meghal.stackmvpapp.features.Search.SearchAdapter;
import mvp.stack.meghal.stackmvpapp.features.Search.Data.SearchData;
import mvp.stack.meghal.stackmvpapp.features.Search.Presenter.SearchPresenter;
import mvp.stack.meghal.stackmvpapp.features.Search.Presenter.SearchPresenterInterface;
import mvp.stack.meghal.stackmvpapp.R;
import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func0;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

import static java.util.Collections.copy;

public class SearchActivity extends AppCompatActivity implements SearchInterface {

    private static final String TAG = "SearchActivity";
    private EditText inputQueryEditText;
    private ProgressBar progressBar;
    private RecyclerView recyclerView;
    private Button searchStackOverFlowButton;

    private List<SearchData> searchDataList = new ArrayList<>();
    private SearchAdapter searchAdapter;
    private SearchPresenterInterface presenterObject;
    private static Handler handler;
    private static boolean firstTap = false;
    private static boolean secondTap = false;
    private boolean textChanged = false;
    private boolean requestSent = false;

    private Observable<Long> observable;
    private Observer<Long> observer;
    private Subscription subscription;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        initializeViews();


        observable = Observable.create(new Observable.OnSubscribe<Long>() {
            @Override
            public void call(Subscriber<? super Long> subscriber) {
                subscriber.onNext((long) 1000);

            }
        });

        observer = new Observer<Long>() {
            @Override
            public void onCompleted() {
                Toast.makeText(SearchActivity.this, "On Completed Called ?", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onError(Throwable e) {
            }

            @Override
            public void onNext(Long aLong) {

                presenterObject.getResults(getInputQuery());
                Log.i(TAG, "Request Sent : " + getInputQuery());

            }
        };


        inputQueryEditText.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


                if (progressBar.getVisibility() == View.GONE) {

                    ProgressbarVisible(true);
                }

            }

            @Override
            public void onTextChanged(final CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(final Editable s) {


                if (subscription == null) {

                } else {
                    if (!subscription.isUnsubscribed()) {
                        subscription.unsubscribe();
                    }

                }
                subscription = observable.delay(1, TimeUnit.SECONDS).subscribe(observer);


            }
        });
        // This is very important this is originally when
/*
        searchStackOverFlowButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recyclerView.setVisibility(View.GONE);
                presenterObject.getResults(getInputQuery());
            }
        });
*/


        searchStackOverFlowButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {


                Log.i(TAG, "On CLick : " + firstTap);
                if (firstTap) {
                    secondTap = true;
                }
                firstTap = true;

                // Observable Create Event
                final Observable<Long> observable = Observable.create(new Observable.OnSubscribe<Long>() {
                    @Override
                    public void call(Subscriber<? super Long> subscriber) {


                        //Toast.makeText(SearchActivity.this,"Call called ?",Toast.LENGTH_LONG).show();


                        subscriber.onNext((long) 0);


                    }
                });


                // Observer Behaviour
                final Observer<Long> observer = new Observer<Long>() {
                    @Override
                    public void onCompleted() {

                        Toast.makeText(SearchActivity.this, "On Completed Called ?", Toast.LENGTH_LONG).show();

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(Long aLong) {
                        if (firstTap && secondTap) {

                            Toast.makeText(SearchActivity.this, "Double Tap" + aLong, Toast.LENGTH_SHORT).show();

                        }
                        firstTap = false;
                        secondTap = false;
                    }
                };

//Scheduler
                Subscription subscription = observable.delay(200, TimeUnit.MILLISECONDS).subscribeOn(AndroidSchedulers.mainThread()).observeOn(AndroidSchedulers.mainThread()).subscribe(observer);


            }


        });


        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(searchAdapter);

    }

    /*
        public void prepareMockData(){
            int i;
            for (i=0;i<10;i++) {
                SearchData data = new SearchData("What is SOF "+i, "Name "+i, i, i, i, "ImageLink");
                searchDataList.add(data);
            }
            searchAdapter.notifyDataSetChanged();
        }
    */


    @Override
    public void showError() {
        Toast.makeText(SearchActivity.this, "Error Occured Unable to Search !", Toast.LENGTH_LONG).show();
    }


    private void initializeViews() {
        inputQueryEditText = (EditText) findViewById(R.id.inputQueryEditText);
        progressBar = (ProgressBar) findViewById(R.id.searchProgressbar);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        searchStackOverFlowButton = (Button) findViewById(R.id.searchStackOverFlow);

        presenterObject = new SearchPresenter(SearchActivity.this, new RetrofitSearchProvider());

        searchAdapter = new SearchAdapter(this);

    }

    @Override
    public void ProgressbarVisible(Boolean visible) {

        if (visible) {


            progressBar.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }
            else {
            searchAdapter=new SearchAdapter(this);
            progressBar.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        }
    }

    public String getInputQuery() {
        return inputQueryEditText.getText().toString();
    }

    @Override
    public void setSearchDataList(List<SearchData> searchDataList) {


        recyclerView.setAdapter(null);
        searchAdapter.setData(searchDataList);
        recyclerView.setAdapter(searchAdapter);
        searchAdapter.notifyDataSetChanged();

    }

}
