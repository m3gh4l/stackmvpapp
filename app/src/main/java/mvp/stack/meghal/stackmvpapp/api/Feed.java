package mvp.stack.meghal.stackmvpapp.api;

import java.util.List;

import mvp.stack.meghal.stackmvpapp.features.Search.Data.SearchData;

/**
 * Created by Meghal on 5/18/2016.
 */
public class Feed {

    public List<SearchData> items;

    public Feed() {

    }

    public List<SearchData> getFeed() {
        return this.items;
    }
    /*public void setFeed(List<SearchData> results) {
        this.items = results;
    }*/
}
