package mvp.stack.meghal.stackmvpapp.features.Search.Model;

import mvp.stack.meghal.stackmvpapp.api.Feed;
import mvp.stack.meghal.stackmvpapp.features.Search.SearchCallback;
import rx.Observable;

/**
 * Created by Meghal on 5/17/2016.
 */
public interface SearchProvider {

/*
    Observable<Feed> getItems(String tag, SearchCallback listener);
*/
    Observable<Feed> getItems(String tag);

}
