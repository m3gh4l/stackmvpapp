package mvp.stack.meghal.stackmvpapp.features.Search.Model;

import android.util.Log;

import java.util.List;

import mvp.stack.meghal.stackmvpapp.api.Feed;
import mvp.stack.meghal.stackmvpapp.api.RequestInterface;
import mvp.stack.meghal.stackmvpapp.api.RequestInterfaceUsingRxJava;
import mvp.stack.meghal.stackmvpapp.features.Search.SearchCallback;
import mvp.stack.meghal.stackmvpapp.features.Search.Data.SearchData;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;
import rx.Observer;
import rx.android.plugins.RxAndroidPlugins;

/**
 * Created by Meghal on 5/17/2016.
 */
public class RetrofitSearchProvider implements SearchProvider {

    private static final String TAG = "RetrofitSearchProvider";


    @Override
    public Observable<Feed> getItems(String query) {


        // Done by jay without using Rx java using Call backs
        /*
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.stackexchange.com")
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        // prepare call in Retrofit 2.0
        RequestInterface request = retrofit.create(RequestInterface.class);
        Call<Feed> call = request.getData();

        call.enqueue(new Callback<Feed>() {
            @Override
            public void onResponse(Call<Feed> call, Response<Feed> response) {
                listener.onTaskCompleted(response.body().getFeed());
                Log.i(TAG, "response available: " + response.message());
            }

            @Override
            public void onFailure(Call<Feed> call, Throwable t) {
                t.printStackTrace();
                listener.onTaskFailed();          }
        });
*/


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.stackexchange.com")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
        final RequestInterfaceUsingRxJava request = retrofit.create(RequestInterfaceUsingRxJava.class);

        final Observable<Feed> observable = request.getData(query);
        return observable;
    }

}


        /*

This is made by Meghal to call api without using rxJava - it uses Callback MEthods

        Call<Feed> call = request.getData(query);

        call.enqueue(new Callback<Feed>() {
            @Override
            public void onResponse(Call<Feed> call, Response<Feed> response) {
                // listener.onTaskCompleted(response.body().getFeed());
                Log.i(TAG, "Response Available : " + response.body().getFeed());
                listener.onTaskCompleted(response.body().getFeed());
            }

            @Override
            public void onFailure(Call<Feed> call, Throwable t) {

                Log.i(TAG, "Error in getting Response " + t.getMessage());
                listener.onTaskFailed();
            }
        });
*/




        /*
        This is created to get data in Json Format !
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                JsonObject data = response.body();
                Log.i(TAG, "data: " + data );
                listener.onTaskCompleted(new ArrayList<SearchData>());
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                listener.onTaskFailed();
                t.printStackTrace();
            }
        });*/


