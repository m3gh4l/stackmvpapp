package mvp.stack.meghal.stackmvpapp.features.Search.Presenter;

import android.util.Log;

import java.util.List;

import mvp.stack.meghal.stackmvpapp.api.Feed;
import mvp.stack.meghal.stackmvpapp.features.Search.Data.SearchData;
import mvp.stack.meghal.stackmvpapp.features.Search.Model.MockSearchProvider;
import mvp.stack.meghal.stackmvpapp.features.Search.Model.SearchProvider;
import mvp.stack.meghal.stackmvpapp.features.Search.SearchCallback;
import mvp.stack.meghal.stackmvpapp.features.Search.View.SearchInterface;
import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Meghal on 5/17/2016.
 */
public class SearchPresenter implements SearchPresenterInterface {

    private static final String TAG = "Search Presenter";
    private SearchInterface searchInterface;

    private Observable<Feed> observable;

    private SearchProvider searchProvider;

    public SearchPresenter(SearchInterface searchInterface, SearchProvider searchProvider) {
        this.searchInterface = searchInterface;
        this.searchProvider = searchProvider;
    }

    @Override
    public void getResults(final String query) {


        searchInterface.ProgressbarVisible(true);
        if (validateQuery(query)) {



            observable=searchProvider.getItems(query);
            observable.observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.newThread()).subscribe(new Observer<Feed>() {
                @Override
                public void onCompleted() {

                }

                @Override
                public void onError(Throwable e) {

                    e.printStackTrace();
                    searchInterface.ProgressbarVisible(false);
                    searchInterface.showError();

                }

                @Override
                public void onNext(Feed feed) {

                    searchInterface.setSearchDataList(feed.getFeed());
                    searchInterface.ProgressbarVisible(false);

                }
            });



            /*
                @Override
                public void onTaskCompleted(List<SearchData> searchDataList) {

                    searchInterface.setSearchDataList(searchDataList);
                    searchInterface.ProgressbarVisible(false);

                }

                @Override
                public void onTaskFailed() {

                    searchInterface.ProgressbarVisible(false);
                    searchInterface.showError();

                }
            });
*/

        } else {

            searchInterface.showError();
            searchInterface.ProgressbarVisible(false);

        }

        //We can handle api calls here !

    }

    public boolean validateQuery(String query) {

        if (query.isEmpty()) {

            return false;

        } else if (query.length() < 4) {

            return false;

        } else {
            return true;
        }

    }


}