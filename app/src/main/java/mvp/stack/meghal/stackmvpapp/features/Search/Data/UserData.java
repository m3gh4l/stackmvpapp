package mvp.stack.meghal.stackmvpapp.features.Search.Data;

/**
 * Created by Meghal on 5/19/2016.
 */
public class UserData {
    private String display_name;
    private String user_id;
    private String profile_image;
    private String reputation;


    UserData(String display_name, String user_id, String profile_image,String reputation) {

        this.user_id = user_id;
        this.profile_image = profile_image;
        this.display_name = display_name;
        this.reputation=reputation;


    }


    public String getDisplay_name() {
        return display_name;
    }

    public String getProfile_image() {
        return profile_image;
    }

    public String getUser_id() {
        return user_id;
    }

    public String getReputation() {
        return reputation;
    }
}
