package mvp.stack.meghal.stackmvpapp.features.UnusedClasses;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Meghal on 5/17/2016.
 */
public interface StackOverFlowApi {

    @GET("/2.2/questions?order=desc&sort=activity&site=stackoverflow")
    Call<StackOverFlowQuestions> loadQuestions(@Query("tagged") String tags);
}
